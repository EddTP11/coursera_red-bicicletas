
Bienvenidos al proyecto RED BICICLETAS
++++++++++++++++++++++++++++++++++++++

Contenido del proyecto
**********************

***************************************************************** SEMANA 4 *************************************************************

1- Obtener el proyecto desde https://bitbucket.org/federicorano/red_bicicletas con el comando "git clone https://bitbucket.org/federicorano/red_bicicletas.git"
2- Posicionarse dentro de la carpeta raiz del proyecto y ejecutar el comando "npm install":
        Ej: red_bicicletas/npm install
        Se instalaran todas las dependencias:
            Ej: nodemon
3- Desarrollo: Posicionarse dentro de la carpeta raiz del proyecto y ejecutar el comando "npm star", el servidor se va a inciar escuchando en el puerto 3000. 
4- Produccion: https://redbicicletasapp.herokuapp.com

API Segurizada (Bicicletas)
***************************
1- En archivo "ApiBicicletas.postman_collection.json" se encuentra el Collection de Postman con los casos de prueba realizados para la conexion con la API de Bicicletas
mediante Autenticacion JWT apuntada al ambiente de PRODUCCION.



********************************************************************* SEMANA 3 ***************************************************************
1- Obtener el proyecto desde https://bitbucket.org/federicorano/red_bicicletas con el comando "git clone https://bitbucket.org/federicorano/red_bicicletas.git"
2- Posicionarse dentro de la carpeta raiz del proyecto y ejecutar el comando "npm install":
        Ej: red_bicicletas/npm install
        Se instalaran todas las dependencias:
            Ej: nodemon
3- Posicionarse dentro de la carpeta raiz del proyecto y ejecutar el comando "npm star", el servidor se va a inciar escuchando en el puerto 3000.


Registrar Usuarios y Autorizarlos  
**********************************
1- Login:
    http://localhost:3000/login
    Se encuentran los dos metodos de autenticacion (Aplicar diferentes formas de autorización utilizando Passport):
        LocalStrategy
        GoogleStrategy
2- Administrador Usuarios:
    http://localhost:3000/usuarios
3- Pagina con validacion de sesion:
    http://localhost:3000/bicicletas


API Segurizada (Bicicletas)
***************************
1- En archivo "ApiBicicletas.postman_collection.json" se encuentra el Collection de Postman con los casos de prueba realizados para la conexion con la API de Bicicletas
mediante Autenticacion JWT.

    
